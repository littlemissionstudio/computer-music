# Mido:

Our main program for converting MIDI messages to and from python objects is
Mido.  You can read the docs for it here: https://mido.readthedocs.io/

# Timidity:

In Ubuntu linux you need a MIDI synth.  You might have one that is "just there",
but if not, we cah use Timidity.


## Quick Setup Guide:

https://blog.thameera.com/playing-midi-files-in-ubuntu/

In Ubuntu the config files are:
 - /etc/timidity/timidity.cfg
 - /etc/timidity/freepats.cfg

The second file gives a list of sounds in that soundbank/soundfont.

## Start Timidity:

```bash
timidity -iA
```

# Simple mido use:

```python
import mido
mido.get_output_names()
# Shortcut name to make code robust against port number changes
midi_out = mido.open_output('TiMidity port 0')

# Play a midi file in the local directory
mfile = mido.MidiFile('test.mid')
for msg in mfile.play():
  midi_out.send(msg)

midi_out.send(mido.Message('note_on', note=64, velocity=100))
midi_out.send(mido.Message('note_off', note=64))

# Change the sound:
midi_out.send(mido.Message('program_change', program=114))
```

# Play notes with library:

```python
from tools import message_interface
message_interface.connect_output_linux()

message_interface.play_note(88, duration=0.5)
for i in range(60, 88):
   message_interface.play_note(i, duration=0.25)
```

# Windows tips:
 - install python 3.9.13
   - As of 26-Oct-2022 python 3.9 is the latest supported version.
   - Download from https://www.python.org/downloads/release/python-3913/
   - Follow the installer
   - Be sure to click "Disable path length limit" at the end of the installer.
 - pygame (used by mido) needs a c++ compiler to install.
   - may not be needed with Python 3.9
   - See https://www.pygame.org/wiki/CompileWindows
   - download https://aka.ms/vs/15/release/vs_buildtools.exe
   - Run the installer and go to "Individual Components"
   - Check "MSVC v140 - VS 2015 C++ build tools (v14.00)"
   - Check "Windows 10 SDK (10.0.20348.0)"
   - Install
   - Relaunch the command prompt

 - install git, including git shell.
 - use git shell to checkout your fork of the project
 - change directory (`cd`) to where you checked out the code
 - Create a virtual environment
   - python -m venv C:\path\to\myenv
 - in windows shell activate your virtualenv
   - myenvenv\Scripts\activate.bat
 - the first time run `pip install -r requirements.txt`
 - launch the python shell, or a write a python script using this library and make music!


# Linux / WSL Tips:
 - sudo apt install libasound2-dev
 - sudo apt install libjack-jackd2-dev