"""
A utility for handling MIDI messages a little more intuitively.
"""
import time

import mido

# Singleton output port.
midi_out = None
# Singleton MidiFile object.
curr_midi_file_obj = None


def connect_output_linux():
    """ This method just means I don't have to remember the port name. """
    global midi_out
    if midi_out is None:
        midi_out = mido.open_output('TiMidity port 0')
    return midi_out


def connect_output_windows():
    """ This method just means I don't have to remember the port name. """
    global midi_out
    if midi_out is None:
        midi_out = mido.open_output()
    return midi_out


def play_note(note_number: int,
              velocity: int = 64,
              duration: float = 1.0) -> None:
    """
    Converts the parameters into a mido MIDI message and plays it.

    Args:
      note_number (int): MIDI note number, 0..127.
      velocity (int): MIDI velocity, 0..127.
      duration (float): How long to hold the note.
    """
    midi_out.send(mido.Message('note_on', note=note_number, velocity=velocity))
    time.sleep(duration)
    midi_out.send(mido.Message('note_off', note=note_number))

    global curr_midi_file_obj
    if curr_midi_file_obj:
        curr_midi_file_obj['midi_track'].append(
            mido.Message('note_on', note=note_number, velocity=velocity,
                         time=0))
        curr_midi_file_obj['midi_track'].append(
            mido.Message(
                'note_off', note=note_number,
                time=round(
                    mido.second2tick(
                        duration,
                        curr_midi_file_obj['midi_file'].ticks_per_beat,
                        curr_midi_file_obj['midi_tempo']))))


def setup_midi_file() -> dict:
    """
    Creates a new MidiFile object with 1 track to write to.

    If a MidiFile object is already open, this will error.
    """
    global curr_midi_file_obj
    if curr_midi_file_obj is not None:
        raise Exception('Already have a midi file object for writing.')
    mf = mido.MidiFile()
    track = mido.MidiTrack()
    mf.tracks.append(track)

    curr_midi_file_obj = {
        'midi_file': mf,
        'midi_track': track,
        'midi_tempo': mido.midifiles.midifiles.DEFAULT_TEMPO,
        'next_note_ticks': int(0)
    }
    return curr_midi_file_obj


def save_midi_file(filepath: str) -> None:
    """
    Write the current midi file object to disk.  If no object exists, raise an
    error.

    Args:
      filepath (str): The path of the file to write.
    """
    global curr_midi_file_obj
    if curr_midi_file_obj is None:
        raise Exception('No MidiFile to write.')

    for m in curr_midi_file_obj['midi_track']:
        print(m.time, type(m.time))
    curr_midi_file_obj['midi_file'].save(filepath)
    curr_midi_file_obj = None
